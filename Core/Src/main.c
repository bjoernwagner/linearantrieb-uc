/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "spi.h"
#include "tim.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbd_cdc_if.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  Ablauf = STAT_INIT;
  Ablauf_Init();//Ablaufinitialisierung

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_TIM2_Init();
  MX_TIM5_Init();
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 2 */

  HAL_TIM_Base_Start(&htim5); // Timer für Delay starten. Läuft immer im Hintergrund
  IBEX_Delay(1000000); // Eine Sekunde für die USB-Initialisierung

  IHM01_Init(); //Motorinitialisierung & "NULLEN"
  Ablauf = STAT_OFFLINE;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */



/*	IBEX_Delay(1000000);
	IHM01_MoveTo(1700, 100, 20000, 20000,1);
	IBEX_Delay(100000);
	IHM01_MoveTo(   0, 500, 20000, 20000,1);
	IBEX_Delay(1000000);
	IHM01_MoveTo(1700, 600, 20000, 20000,1);
	IBEX_Delay(100000);
	IHM01_MoveTo(   0, 1000, 20000, 20000,1);
	IBEX_Delay(1000000);
	IHM01_MoveTo(1700, 2000, 20000, 20000,1);
	IBEX_Delay(100000);
	IHM01_MoveTo(   0, 3000, 20000, 20000,1);
	IBEX_Delay(1000000);
	IHM01_MoveTo(1700, 4000, 20000, 20000,1);
	IBEX_Delay(100000);
	IHM01_MoveTo(   0, 5000, 20000, 20000,1);*/



  while (1)
  	  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	 //Hauptablauf (Basis sind die Nachrichten via USB)
	  switch(Ablauf)
	  	  {
	  	  case STAT_OFFLINE: //Keine Verbindung & keine Kommunikation
	  		  if(USB_Status == CONNECTED) Ablauf = STAT_STANDBY;
	  		  break;

	  	  case STAT_STANDBY: // Verbindung. Bei Kommunikation, auswerten und entscheiden
	  		  if(USB_Status == CONNECTED) Ablauf = STAT_STANDBY;
	  		  else Ablauf = STAT_OFFLINE;
	  		  break;

	  	  case STAT_AUTO_RUN:
	  		  SollMovDat.ZielPos[0]=USB_RxData.Data[0];
	  		  SollMovDat.Speed[0]=USB_RxData.Data[1];
	  		  SollMovDat.Accel[0]=USB_RxData.Data[2];
	  		  SollMovDat.Decel[0]=USB_RxData.Data[3];
	  		  SollMovDat.Delay[0]=USB_RxData.Data[4];
	  		  SollMovDat.ZielPos[1]=USB_RxData.Data[6];
	  		  SollMovDat.Speed[1]=USB_RxData.Data[7];
	  		  SollMovDat.Accel[1]=USB_RxData.Data[8];
	  		  SollMovDat.Decel[1]=USB_RxData.Data[9];
	  		  SollMovDat.Delay[1]=USB_RxData.Data[10];
	  		  SollMovDat.Count[1]=USB_RxData.Data[11];
	  		  SollMovDat.ZielPos[2]=USB_RxData.Data[12];
	  		  SollMovDat.Speed[2]=USB_RxData.Data[13];
	  		  SollMovDat.Accel[2]=USB_RxData.Data[14];
	  		  SollMovDat.Decel[2]=USB_RxData.Data[15];
	  		  SollMovDat.Delay[2]=USB_RxData.Data[16];
	  		  SollMovDat.ZielPos[3]=USB_RxData.Data[18];
	  		  SollMovDat.Speed[3]=USB_RxData.Data[19];
	  		  SollMovDat.Accel[3]=USB_RxData.Data[20];
	  		  SollMovDat.Decel[3]=USB_RxData.Data[21];
	  		  SollMovDat.Delay[3]=USB_RxData.Data[22];
	  		  SollMovDat.Count[3]=USB_RxData.Data[23];
	  		  SollMovDat.Delay[4]=USB_RxData.Data[24];
	  		  SollMovDat.Count[4]=USB_RxData.Data[25];
	  		  Ablauf_Auto();
			  if(USB_Status == CONNECTED) Ablauf = STAT_STANDBY;
			  else Ablauf = STAT_OFFLINE;
	  		  break;

	  	  case STAT_MANU_NULL:
	  		  IHM01_MoveToNull(); //Nullpunkt suchen
	  		  if(USB_Status == CONNECTED) Ablauf = STAT_STANDBY;
	  		  else Ablauf = STAT_OFFLINE;
	  		  break;

	  	  case STAT_MANU_RUN:
	  		  IHM01_MoveTo(USB_RxData.Data[0], USB_RxData.Data[1], USB_RxData.Data[2], USB_RxData.Data[2], USB_RxData.Unit);
	  		  if(USB_Status == CONNECTED) Ablauf = STAT_STANDBY;
	  		  else Ablauf = STAT_OFFLINE;
	  		  break;

	  	  case STAT_ERROR_OC: break;
	  	  case STAT_ERROR_WC: break;
	  	  case STAT_ERROR_TS: break;
	  	  case STAT_ERROR_TW: break;
	  	  case STAT_ERROR_UV: break;
	  	  case STAT_ERROR_UK: break;
	  	  }

	  IBEX_Delay(100);
  	  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 180;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.PLLSAI.PLLSAIM = 16;
  PeriphClkInitStruct.PLLSAI.PLLSAIN = 192;
  PeriphClkInitStruct.PLLSAI.PLLSAIQ = 2;
  PeriphClkInitStruct.PLLSAI.PLLSAIP = RCC_PLLSAIP_DIV4;
  PeriphClkInitStruct.PLLSAIDivQ = 1;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48CLKSOURCE_PLLSAIP;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void Ablauf_Init()
{


}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
