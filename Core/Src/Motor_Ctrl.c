/*
 * Motor_Ctrl.c
 *
 *  Created on: Mar 25, 2020
 *      Author: IBX_DELL_1
 */


#include "Motor_Ctrl.h"


//###################
//### Initialisierung
void IHM01_Init()
{

	CurrMovDat.Unit	= 1;
	CurrMovDat.Delay[4]	= 0;
	CurrMovDat.Count[4]	= 0;
	SollMovDat.Unit	= 1;
	SollMovDat.Delay[4]	= 0;
	SollMovDat.Count[4]	= 0;

	for(uint8_t i=0; i<4; i++)
		{
		CurrMovDat.ZielPos[i]	= 0;
		CurrMovDat.Speed[i]		= 0;
		CurrMovDat.Accel[i]		= 0;
		CurrMovDat.Decel[i]		= 0;
		CurrMovDat.Delay[i]		= 0;
		CurrMovDat.Count[i]		= 0;
		SollMovDat.ZielPos[i]	= 0;
		SollMovDat.Speed[i]		= 0;
		SollMovDat.Accel[i]		= 0;
		SollMovDat.Decel[i]		= 0;
		SollMovDat.Delay[i]		= 0;
		SollMovDat.Count[i]		= 0;
		}

	srd.Run_State = STOP; //Motorflag erstmal auf STOP setzen

	HAL_GPIO_WritePin(IHM01_SPI_CS_GPIO_Port, IHM01_SPI_CS_Pin, GPIO_PIN_SET); //CS  erstmal High setzen (Inaktiv)
	HAL_GPIO_WritePin(IHM01_STBY_GPIO_Port, IHM01_STBY_Pin, GPIO_PIN_SET);	//Board Einschalten
	IBEX_Delay(100000);

	IHM01_SetStepMode(0);	//Stepmode auf Fullstep
	IHM01_GetStatus(); // Alle Fehler erstmal löschen

	IHM01_SetOverCurrentLimit(11); //4,5A
	IHM01_MoveToNull(); //Nullpunkt suchen

}

//###########################
//### Allgemeines Delay in µs
void IBEX_Delay(uint32_t MicroSec)
{
	__HAL_TIM_SET_COUNTER(&htim5,0);
	while (__HAL_TIM_GET_COUNTER(&htim5) <= MicroSec);
}

//#########################
//### Ablauf der Testzyklen
void Ablauf_Auto()
{

	for(uint32_t al = 0 ; al < SollMovDat.Count[4] ; al++) //Wiederholungen über alles
		{
		CurrMovDat.Count[4] = al;
		for(uint32_t il = 0; il < SollMovDat.Count[1] ; il++) //Wiederholungen Move 0 & 1
			{
			CurrMovDat.Count[1] = il;
			if (FLAG_STOP) {FLAG_STOP=FALSE; return;} //Abbruch
			IHM01_MoveTo(SollMovDat.ZielPos[0], SollMovDat.Speed[0], SollMovDat.Accel[0], SollMovDat.Decel[0], SollMovDat.Unit); //Move 0
			if (FLAG_STOP) {FLAG_STOP=FALSE; return;} //Abbruch
			IBEX_Delay(SollMovDat.Delay[0]*1000); //Delay nach Move 0

			if (FLAG_STOP) {FLAG_STOP=FALSE; return;} //Abbruch
			IHM01_MoveTo(SollMovDat.ZielPos[1], SollMovDat.Speed[1], SollMovDat.Accel[1], SollMovDat.Decel[1], SollMovDat.Unit); //Move 1
			if (FLAG_STOP) {FLAG_STOP=FALSE; return;} //Abbruch
			IBEX_Delay(SollMovDat.Delay[1]*1000); //Delay nach Move 1
			}

		for(uint32_t il = 0; il < SollMovDat.Count[3] ; il++) //Wiederholungen Move 2 & 3
			{
			CurrMovDat.Count[3] = il;
			if (FLAG_STOP) {FLAG_STOP=FALSE; return;} //Abbruch
			IHM01_MoveTo(SollMovDat.ZielPos[2], SollMovDat.Speed[2], SollMovDat.Accel[2], SollMovDat.Decel[2], SollMovDat.Unit); //Move 2
			if (FLAG_STOP) {FLAG_STOP=FALSE; return;} //Abbruch
			IBEX_Delay(SollMovDat.Delay[2]*1000); //Delay nach Move 2

			if (FLAG_STOP) {FLAG_STOP=FALSE; return;} //Abbruch
			IHM01_MoveTo(SollMovDat.ZielPos[3], SollMovDat.Speed[3], SollMovDat.Accel[3], SollMovDat.Decel[3], SollMovDat.Unit); //Move 3
			if (FLAG_STOP) {FLAG_STOP=FALSE; return;} //Abbruch
			IBEX_Delay(SollMovDat.Delay[3]*1000); //Delay nach Move 3
			}
		if (FLAG_STOP) {FLAG_STOP=FALSE; return;} //Abbruch
		IBEX_Delay(SollMovDat.Delay[4]*1000); //Delay zwischen den Komplettwiederholungen
		}
}

//#####################
//### Motor einschalten
void IHM01_MotorOn()
{
	IHM01_PowerStageEnable(TRUE);   //Endstufe einschalten
	IBEX_Delay(10000);
	IHM01_StartPwm();				//PWM einschalten
}

//#####################
//### Motor ausschalten
void IHM01_MotorOff()
{
	IHM01_StopPwm();				//PWM ausschalten
	IBEX_Delay(10000);
	IHM01_PowerStageEnable(FALSE);  //Endstufe ausschalten
}

//################
// Position Nullen
void IHM01_MoveToNull()
{

	//Wenn schon Endlagenschalter, erstmal 100 Steps in die andere Richtung fahren
	HAL_NVIC_DisableIRQ(EXTI1_IRQn);
	if(!HAL_GPIO_ReadPin(IHM01_ELS_IN_GPIO_Port, IHM01_ELS_IN_Pin))
		{
		IHM01_IstPosSW = 100;
		IHM01_MoveTo(0, 2000, 10000, 10000,1);
		}

	IBEX_Delay(100000);

	//Fahren bis Enlagenschalter. Stop kommt vom ELS-Interrupt
	HAL_NVIC_EnableIRQ(EXTI1_IRQn);
	IHM01_IstPosSW = 0;
	IHM01_MoveTo(2000, 1000, 10000, 10000,1);

	//Stück vom Endlagenschalter wegfahren. Letzter anfahrbarer Punkt
	HAL_NVIC_DisableIRQ(EXTI1_IRQn);
	IBEX_Delay(100000);

	//Neue Nullposition anfahren und Ende
	IHM01_IstPosSW = 1765;
	IHM01_MoveTo(0, 1000, 10000, 10000,1);
	IBEX_Delay(100000);

	HAL_NVIC_EnableIRQ(EXTI1_IRQn);
}

//#########################################################################################
//### Fahre an die Position mit der Geschwindigkeit und Beschleunigung.
//### Hier nur die Berechnungen. Ablauf im HAL_TIM_PWM_PulseFinishedCallback von Timer2
//### Units(v & a): 0: 0,01*RAD, 1:Steps, 2:mm 3:mg/mm
void IHM01_MoveTo(uint16_t ZielPos, uint16_t Speed, uint16_t Accel, uint16_t Decel, uint8_t Units)
{
	unsigned int max_s_lim;
	unsigned int accel_lim;
	int32_t Steps;

	/*### Units ###
	 * 0: Steps & Rad/s   & Rad/s2  (nie verwendet)
	 * 1: Steps & Steps/s & Steps/s2
	 * 2: mm    & mm/s    & mm/s2
	 * 3: mm    & mm/s    & mg			 */


	switch(Units)
		{
		case 0:	// in 0,01 RAD. Außer Korrekturfaktoren, nichts machen ,da der Algo schon mit Steps & RAD arbeitet
			Steps = ZielPos - IHM01_IstPosSW; 	//Rel. Weg = Zielposition - Istposition;
			Accel = (uint32_t) (Accel * ACCEL_CORR);
			Decel = (uint32_t) (Decel * DECEL_CORR);
			break;

		case 1: // Steps/s und Steps/s2 in 0,01 RAD umrechnen. Steps bleiben Steps
			Steps = ZielPos - IHM01_IstPosSW; 									//Rel. Weg = Zielposition - Istposition;
			Speed = (uint32_t) ((Speed * 2 * 3.14159 * 100) 			 / FSPR); 	//Umrechnung n/s  in 0,01 RAD/s
			Accel = (uint32_t) ((Accel * 2 * 3.14159 * 100 * ACCEL_CORR) / FSPR); 	//Umrechnung n/s2 in 0,01 RAD/s2
			Decel = (uint32_t) ((Decel * 2 * 3.14159 * 100 * DECEL_CORR) / FSPR); 	//Umrechnung n/s2 in 0,01 RAD/s2
			break;

		case 2: // mm/s und mm/s2 in 0,01 RAD umrechnen. mm in Steps
			Steps = ( int32_t) (((ZielPos * FSPR) / mmPR)- IHM01_IstPosSW); 	  //Rel. Weg = Zielposition(Umrechnung mm in Steps) - Istposition;
			Speed = (uint32_t) ((Speed * 2 * 3.14159 * 100)				 / mmPR); //Umrechnung mm/s  in 0,01 RAD/s
			Accel = (uint32_t) ((Accel * 2 * 3.14159 * 100 * ACCEL_CORR) / mmPR); //Umrechnung mm/s2 in 0,01 RAD/s2
			Decel = (uint32_t) ((Decel * 2 * 3.14159 * 100 * DECEL_CORR) / mmPR); //Umrechnung mm/s2 in 0,01 RAD/s2
			break;

		case 3: // mm/s und mg in 0,01 RAD umrechnen
			Steps = ( int32_t) (((ZielPos * FSPR) / mmPR)- IHM01_IstPosSW); 	  			 //Rel. Weg = Zielposition(Umrechnung mm in Steps) - Istposition;
			Speed = (uint32_t) ((Speed * 2 * 3.14159 * 100)							/ mmPR); //Umrechnung mm/s in RAD/s
			Accel = (uint32_t) ((Accel * 9.806652 * 2 * 3.14159 * 100 * ACCEL_CORR) / mmPR); //Umrechnung mg in RAD/s2
			Decel = (uint32_t) ((Decel * 9.806652 * 2 * 3.14159 * 100 * ACCEL_CORR) / mmPR); //Umrechnung mg in RAD/s2
			break;
		}

	//Regler nach Geschwindigkeit einstellen (Achtung, ab hier alles in RAD!!!)
	if( Speed <= 1500)						IHM01_SetPhaseCurrentControl(10); //0,3125A
	if((Speed >  1500) && (Speed <=  3000))	IHM01_SetPhaseCurrentControl(15); //0,468755A
	if((Speed >  3000) && (Speed <=  6000))	IHM01_SetPhaseCurrentControl(20); //0,625A
	if((Speed >  6000) && (Speed <= 12000))	IHM01_SetPhaseCurrentControl(30); //0,9375A
	if( Speed > 12000)						IHM01_SetPhaseCurrentControl(40); //1,25A

	//Richtung Festlegen
	if(Steps < 0)	IHM01_SetDirection(RETRACT);	// Wenn Retract (-)
	else			IHM01_SetDirection(EXTEND);

	Steps = abs (Steps); //Ab hier nur noch positiv rechnen

	//Es wird bei Speed = 0 davon ausgegangen, daß die Position nicht wirklich angefahren werden soll. Also überspringen.
	if (Speed == 0) Steps = 0;

	if (Accel == 0) Accel = 10;
	if (Decel == 0) Decel = 10;

	// Wenn nur 1 Step Bewegung
//	if(Steps == 1)
//		{
//		srd.AccelDecel_Count = -1; //Ein Schritt...
//		srd.Run_State = DECEL; //... im Bremsmodus
//		srd.Next_Step_Delay = 1000; // Bei einem Step braucht nichts berechnet werden
//		IHM01_SetPWMTimer(srd.Next_Step_Delay);
//		IHM01_MotorOn();
//		}
//	else if(Steps != 0)
	if(Steps >= 3) // Nur wenn Anzahl der Schritte größer 2
		{
		// Set max speed limit, by calc min_delay to use in timer.
		// min_delay = (alpha / tt)/ w
		srd.Min_Delay = A_T_x100 / Speed;

		// Set accelration by calc the first (c0) step delay .
		// step_delay = 1/tt * sqrt(2*alpha/accel)
		// step_delay = ( tfreq*0.676/100 )*100 * sqrt( (2*alpha*10000000000) / (accel*100) )/10000
		srd.Next_Step_Delay = (T1_FREQ_148 * sqrt(A_SQ / Accel))/100;

		// Find out after how many steps does the speed hit the max speed limit.
	    // max_s_lim = speed^2 / (2*alpha*accel)
	    max_s_lim = (long)Speed*Speed/(long)(((long)A_x20000 * Accel)/100);
	    // If we hit max speed limit before 0,5 step it will round to 0.
	    // But in practice we need to move atleast 1 step to get any speed at all.
	    if(max_s_lim == 0)	max_s_lim = 1;

	    // Find out after how many steps we must start deceleration.
	    // n1 = (n1+n2)decel / (accel + decel)
	    accel_lim = ((long)Steps * Decel) / (Accel + Decel);
	    // We must accelerate at least 1 step before we can start deceleration.
	    if(accel_lim == 0)	accel_lim = 1;

	    // Use the limit we hit first to calc decel.
	    if(accel_lim <= max_s_lim)	srd.Decel_Val = accel_lim - Steps;
	    else	    				srd.Decel_Val = -(uint32_t)((max_s_lim * Accel) / Decel);

	    // We must decelerate at least 1 step to stop.
	    if(srd.Decel_Val == 0)		srd.Decel_Val = -1;

	    // Find step to start deceleration.
	    srd.Decel_Start_Pos = Steps + srd.Decel_Val - 1;

	    // If the maximum speed is so low that we dont need to go via acceleration state.
	    if(srd.Next_Step_Delay <= srd.Min_Delay)
	    	{
	    	srd.Next_Step_Delay = srd.Min_Delay;
	    	srd.Run_State = MOVE;
	     	}
	    else
	    	{
	    	srd.Run_State = ACCEL;
	     	}

	    // Reset counter
	    srd.AccelDecel_Count = 0;
	    IHM01_SetPWMTimer(srd.Next_Step_Delay);
	    IHM01_MotorOn();
	    }

	while(srd.Run_State != STOP); // Blocken, solange der Motor noch in Bewegung ist
}

//#######################################
//### Limit für Überstromerkennung setzen
void IHM01_SetOverCurrentLimit(uint8_t value)
{
	/* 4Bit. Ein LSB = 375mA
	0:	375mA
	1:	750mA
	...
	8:	3,375A (Default)
	...
	15:	6A					*/

	if(value > 15)	value = 8; //Bei ungültigen Wert

	IHM01_SPI_TX_Data[0] = 0x13; // OCD-Treshold
	IHM01_SPI_TX_Data[1] = value;
	SPI_TXRX(2);
}

//######################################
//### Limit für Überstromerkennung lesen
void IHM01_GetOverCurrentLimit()
{
	IHM01_SPI_TX_Data[0] = 0x33; // OCD-Treshold
	IHM01_SPI_TX_Data[1] = 0x00;
	SPI_TXRX(2);
}

//###########################################
//### Limit für Phasenstrom setzen (Regelung)
void IHM01_SetPhaseCurrentControl(uint8_t value)
{
	/* 7Bit. Ein LSB = 31,25mA
	  0:	31,25mA
	  1:	62,5mA
	 ...
	 41:	1,3125A (Default)
	...
	127:	4A					*/

	if(value > 127)	value = 41; //Bei ungültigen Wert
	IHM01_SPI_TX_Data[0] = 0x09; // Torque Regulation Value (TVAL)
	IHM01_SPI_TX_Data[1] = value;
	SPI_TXRX(2);
}

//##########################################
//### Limit für Phasenstrom lesen (Regelung)
void IHM01_GetPhaseCurrentControl()
{
	IHM01_SPI_TX_Data[0] = 0x29; // Torque Regulation Value (TVAL)
	IHM01_SPI_TX_Data[1] = 0x00;
	SPI_TXRX(2);
}

//#################################################
//### Stepmode setzen (Zwischenschritte 0,1,3,7,15)
void IHM01_SetStepMode(uint8_t value)
{

	IHM01_SPI_TX_Data[0] = 0x16; // Stepmode

	switch (value)
	{
		case 0:
			IHM01_SPI_TX_Data[1] = 0x88; //Voll-Step
			break;
		case 2:
			IHM01_SPI_TX_Data[1] = 0x89; //Halb-Step
			break;
		case 4:
			IHM01_SPI_TX_Data[1] = 0x8A; //1/4-Step
			break;
		case 8:
			IHM01_SPI_TX_Data[1] = 0x8B; //1/8-Step
			break;
		case 16:
			IHM01_SPI_TX_Data[1] = 0x8C; //1/16-Step
			break;
		default:
			IHM01_SPI_TX_Data[1] = 0x88; //Voll-Step
			break;
	}
	SPI_TXRX(2);
}

//################################
//### Drehrichtung Motor festlegen
void IHM01_SetDirection(int8_t Dir)
{
	switch (Dir)
	{
		case RETRACT:
			HAL_GPIO_WritePin(GPIOA, IHM01_DIR_Pin, GPIO_PIN_RESET);
			srd.Direction = RETRACT;
			break;

		case EXTEND:
			HAL_GPIO_WritePin(GPIOA, IHM01_DIR_Pin, GPIO_PIN_SET);
			srd.Direction = EXTEND;
			break;

		default:
			break;
	}


}

//###################
//### Endstufe ON/OFF
void IHM01_PowerStageEnable(uint8_t value)
{
	if(value == 1)		IHM01_SPI_TX_Data[0] = 0xB8; // Endstufe ON
	else				IHM01_SPI_TX_Data[0] = 0xA8; // Endstufe OFF

	SPI_TXRX(1);
}

//############################
//### Aktuelle Position setzen
void IHM01_SetPosHW(uint32_t value)
{
	IHM01_SPI_TX_Data[0] = 0x01;
	IHM01_SPI_TX_Data[1] = value >> 16;
	IHM01_SPI_TX_Data[2] = value >> 8;
	IHM01_SPI_TX_Data[3] = value;

	SPI_TXRX(4);
}

//##############################
//### Aktuelle Position auslesen
uint16_t IHM01_GetPosHW()
{
	IHM01_SPI_TX_Data[0] = 0x21; //GetParam auf Adresse 0x01
	IHM01_SPI_TX_Data[1] = 0x00;
	IHM01_SPI_TX_Data[2] = 0x00;
	IHM01_SPI_TX_Data[3] = 0x00;

	SPI_TXRX(4); //Position beim Motortreiber anfragen

	int32_t IHM01_CurrentPos_HW = ((IHM01_SPI_RX_Data[1] << 16) | (IHM01_SPI_RX_Data[2] << 8) | (IHM01_SPI_RX_Data[3]));
	if(IHM01_CurrentPos_HW >= 2097151) IHM01_CurrentPos_HW |= 0xffc00000;	//Vorzeichenwechsel bei 22Bit (11111111110000000000000000000000‬)

return (uint16_t) IHM01_CurrentPos_HW;
}

//#############################
//### Status & Fehler  auslesen

uint16_t IHM01_GetStatus()
{
uint16_t ret;

	IHM01_SPI_TX_Data[0] = 0xD0; //GetStatus
	IHM01_SPI_TX_Data[1] = 0x00;
	IHM01_SPI_TX_Data[2] = 0x00;
	SPI_TXRX(3);

	ret = ((IHM01_SPI_RX_Data[1] << 8) | IHM01_SPI_RX_Data[2]);

return ret;
}
