/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "string.h"
#include "stdlib.h"
#include "ctype.h"
#include "Motor_Ctrl.h"
#include "math.h"

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
uint8_t Ablauf;
volatile uint8_t USB_Status;

void Ablauf_Init();
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define B1_EXTI_IRQn EXTI15_10_IRQn
#define IHM01_STATEOUT_Pin GPIO_PIN_0
#define IHM01_STATEOUT_GPIO_Port GPIOC
#define IHM01_ELS_IN_Pin GPIO_PIN_1
#define IHM01_ELS_IN_GPIO_Port GPIOA
#define IHM01_ELS_IN_EXTI_IRQn EXTI1_IRQn
#define IHM01_SPI_SCK_Pin GPIO_PIN_5
#define IHM01_SPI_SCK_GPIO_Port GPIOA
#define IHM01_SPI_MISO_Pin GPIO_PIN_6
#define IHM01_SPI_MISO_GPIO_Port GPIOA
#define IHM01_SPI_MOSI_Pin GPIO_PIN_7
#define IHM01_SPI_MOSI_GPIO_Port GPIOA
#define IHM01_STBY_Pin GPIO_PIN_7
#define IHM01_STBY_GPIO_Port GPIOC
#define IHM01_DIR_Pin GPIO_PIN_8
#define IHM01_DIR_GPIO_Port GPIOA
#define IMH01_FLAG_Pin GPIO_PIN_10
#define IMH01_FLAG_GPIO_Port GPIOA
#define IMH01_FLAG_EXTI_IRQn EXTI15_10_IRQn
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define IHM01_SPI_CS_Pin GPIO_PIN_6
#define IHM01_SPI_CS_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define TRUE 1
#define FALSE 0

#define DISCONNECTED 0 //USB nicht verbunden
#define CONNECTED    1 //USB verbunden

#define STAT_INIT		99	//Noch Initialisierung
#define STAT_OFFLINE 	0	// Keine Kommunikation
#define STAT_STANDBY 	1	// Kommunikation, kein Befehl
#define STAT_AUTO_RUN 	2	// Automatischer Ablauf läuft
//#define STAT_AUTO_PAUSE	3	// Pause Automatik
//#define STAT_AUTO_STOP	4	// Stop Automatik
#define STAT_MANU_RUN 	5	// Läuft noch manuell
#define STAT_MANU_NULL 	6	// Noch beim "Nullen"
#define STAT_ERROR_WC	11	// Wrong Command Fehler erkannt
#define STAT_ERROR_UV	12	// Undervoltage Fehler erkannt
#define STAT_ERROR_TW	13	// Temp Warning Fehler erkannt
#define STAT_ERROR_TS	14	// Temp Shutdown Fehler erkannt
#define STAT_ERROR_OC	15	// Overcurrent Fehler erkannt
#define STAT_ERROR_UK	16	// Fehler unbekannt

#define CMD_REQUEST 	 1
#define CMD_AUTO_START 	 2
#define CMD_AUTO_PAUSE	 3
#define CMD_AUTO_STOP 	 4
#define CMD_MANU_NEWPOS  5
#define CMD_MANU_NULLPOS 6
#define CMD_RESET_ERROR	 7

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
