/*
 * Motor_Ctrl.h
 *
 *  Created on: Mar 25, 2020
 *      Author: IBX_DELL_1
 */

#ifndef INC_MOTOR_CTRL_H_
#define INC_MOTOR_CTRL_H_

#include "gpio.h"
#include "spi.h"
#include "tim.h"

//Richtung
#define EXTEND	1
#define RETRACT	0

//Motordaten
#define FSPR 200 //Anzahl Full Steps per Round
#define mmPR 54  //Anzahl mm per Round

//Timerfrequenz & Korrekturfaktoren
#define T1_FREQ 987500 //Ausgemessen bei 90MHz/90Prescaler
#define ACCEL_CORR 0.955 //Ausgemessen. Beschleunigung ist n bissl zu groß
#define DECEL_CORR 0.90 //Ausgemessen. Bremsen ist n bissl zu groß


//Mathematische Konstanten
#define ALPHA (2*3.14159/FSPR)                 // 2*pi/spr --> Schrittweite in RAD
#define A_T_x100 ((long)(ALPHA*T1_FREQ*100))     // (ALPHA / T1_FREQ)*100
#define T1_FREQ_148 ((int32_t)((T1_FREQ*0.676)/100)) // divided by 100 and scaled by 0.676
#define A_SQ (long)(ALPHA*2*10000000000)         // ALPHA*2*10000000000
#define A_x20000 (int)(ALPHA*20000)              // ALPHA*20000

//Zustände für die Moving-Statemachine
#define STOP	0 // Keine Bewegung
#define ACCEL	1 // Beschleunigen
#define DECEL	2 // Abbremsen
#define MOVE	3 // Gleichförmige Bewegung

uint8_t FLAG_STOP;
uint16_t IHM01_IstPosSW; //Istposition des Motors

typedef struct {
	uint8_t 	Run_State;   		// Zustand State-Machine
	uint8_t 	Direction; 	 		// Richtung EXTEND oder RETRACT
	uint32_t 	Next_Step_Delay;	// Wartezeit für den nächsten Step
	uint32_t 	Decel_Start_Pos;	// StepPosition bei Bremsbegin
	int32_t 	Decel_Val;	 		// Bremswert
	int32_t 	Min_Delay; 	 		// Minimum Delay --> Maximale Geschwindigkeit
	int32_t 	AccelDecel_Count;	// Zähler für Beschleunigung/Bremsen. Zur berechnung von Step_Delay
} SpeedRampData;
SpeedRampData srd;

typedef struct {
	uint16_t 	ZielPos[4];// Wohin
	uint16_t	Speed[4];	// Geschwindigkeit
	uint16_t 	Accel[4];	// Beschleunigung
	uint16_t 	Decel[4];	// Bremsen
	uint8_t 	Unit;	// Welche Units
	uint16_t 	Delay[5];	// Pause nach dem Move
	uint16_t 	Count[5];	// Anzahl Wiederholungen
} MovingData;
MovingData SollMovDat;
MovingData CurrMovDat;



void IBEX_Delay(uint32_t MicroSec);

void IHM01_Init();


void IHM01_SetDirection(int8_t Dir);

void IHM01_PowerStageEnable(uint8_t value);
void IHM01_MotorOn();
void IHM01_MotorOff();

uint16_t IHM01_GetPosHW();
uint16_t IHM01_GetStatus();
void IHM01_SetStepMode(uint8_t value);
void IHM01_SetPosHW(uint32_t value);
void IHM01_SetOverCurrentLimit(uint8_t value);
void IHM01_GetOverCurrentLimit();
void IHM01_SetPhaseCurrentControl(uint8_t value);
void IHM01_GetPhaseCurrentControl();

void Ablauf_Auto();
void IHM01_MoveTo(uint16_t ZielPos, uint16_t Speed, uint16_t Accel, uint16_t Decel, uint8_t Units);
void IHM01_MoveToNull();


#endif /* INC_MOTOR_CTRL_H_ */
